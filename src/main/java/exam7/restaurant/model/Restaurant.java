package exam7.restaurant.model;


import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.UUID;

@Document(collection = "restaurants")
@Data
public class Restaurant {

    @Id
    private String id = UUID.randomUUID().toString();

    private String name;
    private String description;

    public Restaurant(String name, String description) {
        this.name = name;
        this.description = description;
    }
}
