package exam7.restaurant.model;


import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.UUID;

@Document(collection = "dishes")
@Data
public class Dish {

    @Id
    private String id = UUID.randomUUID().toString();

    @DBRef
    private Restaurant restaurant;

    private String name;
    private String type;
    private int price;

    public Dish(Restaurant restaurant, String name, String type, int price) {
        this.restaurant = restaurant;
        this.name = name;
        this.type = type;
        this.price = price;
    }
}
