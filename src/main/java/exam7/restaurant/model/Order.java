package exam7.restaurant.model;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;
import java.util.UUID;

@Document(collection = "orders")
@Data
public class Order {

    @Id
    private String id = UUID.randomUUID().toString();

    @DBRef
    private Client client;
    @DBRef
    private Dish dish;

    private LocalDateTime ldt_order;

    public Order(Client client, Dish dish) {
        this.client = client;
        this.dish = dish;
        ldt_order = LocalDateTime.now();
    }
}
