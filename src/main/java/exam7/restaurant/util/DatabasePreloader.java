package exam7.restaurant.util;

import exam7.restaurant.model.Client;
import exam7.restaurant.model.Dish;
import exam7.restaurant.model.Order;
import exam7.restaurant.model.Restaurant;
import exam7.restaurant.repository.ClientRepository;
import exam7.restaurant.repository.DishRepository;
import exam7.restaurant.repository.OrderRepository;
import exam7.restaurant.repository.RestaurantRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@Configuration
public class DatabasePreloader {
    private final Random r = new Random();
    private final BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();


    @Bean
    CommandLineRunner initDatabase(RestaurantRepository rr, DishRepository dr, OrderRepository or, ClientRepository cr){
        return(args) -> {
            rr.deleteAll();
            dr.deleteAll();
            or.deleteAll();
            cr.deleteAll();

            List<Restaurant> restaurants = new ArrayList<>();
            List<Dish> dishes = new ArrayList<>();
            List<Order> orders = new ArrayList<>();
            List<Client> clients = new ArrayList<>();

            int restaurantAmount = r.nextInt(3) + 2;
            int dishAmount = r.nextInt(10) + 5;
            int orderAmount = r.nextInt(5) + 3;
            int clientAmount = r.nextInt(3) + 2;


            //adding restaurants

            for(int i = 0; i < restaurantAmount; i++){
                GenerateData.PlaceName randomPlace = GenerateData.randomPlace();

                Restaurant rest = new Restaurant(randomPlace.name.trim(), randomPlace.description.trim());
                restaurants.add(rest);
            }
            rr.saveAll(restaurants);


            //adding dishes with restaurants inside

            for(int i = 0; i < dishAmount; i++){
                GenerateData.DishName randomDish = GenerateData.randomDish();
                Restaurant randomRest = restaurants.get(r.nextInt(restaurants.size()));
                int randomPrice = r.nextInt(100) + 30;

                Dish d = new Dish(randomRest, randomDish.name.trim(), randomDish.type.trim(), randomPrice);
                dishes.add(d);
            }
            dr.saveAll(dishes);


            //adding clients

            for(int i = 0; i < clientAmount; i++){
                Client client = new Client(GenerateData.randomPersonName(),
                                            GenerateData.randomEmail(),
                                            encoder.encode("123"));
                clients.add(client);
            }
            cr.saveAll(clients);


            //adding orders with clients and dishes inside

            for(int i = 0; i < orderAmount; i++){
                Dish randomDish = dishes.get(r.nextInt(dishes.size()));
                Client randomClient = clients.get(r.nextInt(clients.size()));

                Order o = new Order(randomClient, randomDish);
                orders.add(o);
            }
            or.saveAll(orders);
        };
    }
}