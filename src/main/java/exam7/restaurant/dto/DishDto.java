package exam7.restaurant.dto;

import exam7.restaurant.model.Dish;
import lombok.*;

@Data
@Builder(access = AccessLevel.PRIVATE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE, force = true)
public class DishDto {
    private String name;
    private int price;

    public static DishDto from(Dish dish){
        return builder()
                .name(dish.getName())
                .price(dish.getPrice())
                .build();
    }
}
