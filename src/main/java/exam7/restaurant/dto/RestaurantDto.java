package exam7.restaurant.dto;

import exam7.restaurant.model.Restaurant;
import lombok.*;

@Data
@Builder(access = AccessLevel.PRIVATE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE, force = true)
public class RestaurantDto {
    private String name;
    private String description;

    public static RestaurantDto from(Restaurant rest){
        return builder()
                .name(rest.getName())
                .description(rest.getDescription())
                .build();
    }
}