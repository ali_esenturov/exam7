package exam7.restaurant.dto;

import exam7.restaurant.model.Order;
import lombok.*;
import java.time.format.DateTimeFormatter;

@Data
@Builder(access = AccessLevel.PRIVATE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE, force = true)
public class OrderDto {
    private String order_time;
    private String dish_name;
    private String restaurant_name;
    private static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");


    public static OrderDto from(Order order){
        return builder()
                .dish_name(order.getDish().getName())
                .restaurant_name(order.getDish().getRestaurant().getName())
                .order_time(order.getLdt_order().format(formatter))
                .build();
    }
}
