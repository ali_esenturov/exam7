package exam7.restaurant.service;

import exam7.restaurant.model.Client;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserAuthService implements UserDetailsService {

    private final ClientService clientService;

    public UserAuthService(ClientService clientService) {
        this.clientService = clientService;
    }

    @Override
    public Client loadUserByUsername(String s) throws UsernameNotFoundException {
        Optional<Client> client  = Optional.ofNullable(clientService.getClientByEmail(s));

        if(client.isEmpty()) {
            throw new UsernameNotFoundException("User not found");
        }

        return client.get();
    }
}
