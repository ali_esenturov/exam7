package exam7.restaurant.service;

import exam7.restaurant.dto.RestaurantDto;
import exam7.restaurant.repository.RestaurantRepository;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.stereotype.Service;
import springfox.documentation.annotations.ApiIgnore;

@Service
public class RestaurantService {

    private final RestaurantRepository rp;

    public RestaurantService(RestaurantRepository rp) {
        this.rp = rp;
    }

    public Slice<RestaurantDto> findAllRestaurants(@ApiIgnore Pageable pageable){
        return rp.findAll(pageable).map(RestaurantDto::from);
    }
}