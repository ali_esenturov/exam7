package exam7.restaurant.service;

import exam7.restaurant.dto.OrderDto;
import exam7.restaurant.model.Client;
import exam7.restaurant.model.Dish;
import exam7.restaurant.model.Order;
import exam7.restaurant.repository.ClientRepository;
import exam7.restaurant.repository.DishRepository;
import exam7.restaurant.repository.OrderRepository;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class OrderService {

    private final OrderRepository or;
    private final ClientRepository cr;
    private final DishRepository dr;

    public OrderService(OrderRepository or, ClientRepository cr, DishRepository dr) {
        this.or = or;
        this.cr = cr;
        this.dr = dr;
    }

    public List<OrderDto> findOrdersByClient(Authentication authentication){
        String clientId = getClientFromAuth(authentication);

        return or.findAllByClientId(clientId)
                .stream()
                .map(OrderDto::from)
                .collect(Collectors.toCollection(ArrayList::new));
    }

    public OrderDto addOrder(Authentication authentication, String dishId){
        String clientId = getClientFromAuth(authentication);
        Client c = cr.getById(clientId);
        Dish d = dr.getById(dishId);

        Order o = new Order(c,d);
        or.save(o);

        return OrderDto.from(o);
    }

    private String getClientFromAuth(Authentication authentication){
        Client c = (Client)authentication.getPrincipal();
        return c.getId();
    }
}
