package exam7.restaurant.service;

import exam7.restaurant.model.Client;
import exam7.restaurant.repository.ClientRepository;
import org.springframework.stereotype.Service;

@Service
public class ClientService {
    private final ClientRepository cr;

    public ClientService(ClientRepository cr) {
        this.cr = cr;
    }

    public Client getClientByEmail(String email){
        return cr.getClientByEmail(email);
    }

    public Client getClientById(String id){
        return cr.getById(id);
    }
}
