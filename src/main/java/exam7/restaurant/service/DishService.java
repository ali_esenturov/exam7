package exam7.restaurant.service;

import exam7.restaurant.dto.DishDto;
import exam7.restaurant.repository.DishRepository;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.stereotype.Service;
import springfox.documentation.annotations.ApiIgnore;


@Service
public class DishService {

    private final DishRepository dr;

    public DishService(DishRepository dr) {
        this.dr = dr;
    }

    public Slice<DishDto> findAllDishesByRestaurant(@ApiIgnore Pageable pageable, String restaurantId){
        return dr.findAllByRestaurantId(pageable, restaurantId).map(DishDto::from);
    }
}