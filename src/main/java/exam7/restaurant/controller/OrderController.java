package exam7.restaurant.controller;

import exam7.restaurant.annotations.ApiPageable;
import exam7.restaurant.dto.OrderDto;
import exam7.restaurant.service.ClientService;
import exam7.restaurant.service.OrderService;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@ApiPageable
@RestController
@RequestMapping("/orders")
public class OrderController {
    private final OrderService os;
    private final ClientService cs;

    public OrderController(OrderService os, ClientService cs) {
        this.os = os;
        this.cs = cs;
    }

    @GetMapping("/my_orders")
    public List<OrderDto> findOrdersByClient(Authentication authentication){
        return os.findOrdersByClient(authentication);
    }

    @PostMapping("/add/{dishId}")
    public OrderDto addOrder(Authentication authentication, @PathVariable String dishId){
        return os.addOrder(authentication, dishId);
    }
}
