package exam7.restaurant.controller;

import exam7.restaurant.annotations.ApiPageable;
import exam7.restaurant.dto.RestaurantDto;
import exam7.restaurant.service.RestaurantService;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

@ApiPageable
@RestController
@RequestMapping("/restaurants")
public class RestaurantController {
    private final RestaurantService rs;

    public RestaurantController(RestaurantService rs) {
        this.rs = rs;
    }

    @GetMapping()
    public Slice<RestaurantDto> findAll(@ApiIgnore Pageable pageable){
        return rs.findAllRestaurants(pageable);
    }
}
