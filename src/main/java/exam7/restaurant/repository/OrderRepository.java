package exam7.restaurant.repository;


import exam7.restaurant.model.Order;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface OrderRepository extends CrudRepository<Order, String> {
    List<Order> findAllByClientId(String clientId);
}