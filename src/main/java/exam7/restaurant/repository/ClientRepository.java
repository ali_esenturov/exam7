package exam7.restaurant.repository;

import exam7.restaurant.model.Client;
import org.springframework.data.repository.CrudRepository;

public interface ClientRepository extends CrudRepository<Client, String> {
    Client getClientByEmail(String email);
    Client getById(String id);
}
