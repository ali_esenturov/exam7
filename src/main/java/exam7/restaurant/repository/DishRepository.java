package exam7.restaurant.repository;

import exam7.restaurant.model.Dish;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import springfox.documentation.annotations.ApiIgnore;


public interface DishRepository extends PagingAndSortingRepository<Dish, String> {
    Page<Dish> findAllByRestaurantId(@ApiIgnore Pageable pageable, String restaurantId);
    Dish getById(String id);
}